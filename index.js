/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Index from './src/pages/Login';
import {name as appName} from './app.json';
import  AppNavigator  from './src/pages/Routes'
AppRegistry.registerComponent(appName, () => AppNavigator);
