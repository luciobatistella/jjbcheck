import { create} from 'apisauce'
import {AsyncStorage} from 'react-native'


const api = create({
    baseURL:'http://198.211.113.20/'
   // baseURL:'http://192.168.0.15:3333/api'
})

api.addAsyncRequestTransform(request => async () => {
    const token = await AsyncStorage.getItem('@CodeApi:myToken')
    const myToken = token

    if(token){
        console.log(myToken);
        request.headers['Authorization'] = `Bearer ${myToken}`
    }else{
        console.log('sem token');
    }

    
    
  
})

api.addResponseTransform(response => {
    if(!response.ok) throw response
})

export default api;