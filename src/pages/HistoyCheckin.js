import React, { Component } from 'react';
import { SafeAreaView, Text, FlatList, View, Image, ActivityIndicator } from 'react-native';

import styles from '../assets/css/HistoryCheckin';


export default class Login extends Component{


    constructor(props){
        super(props);
        this.state ={
            data: []
        }
    }

    static navigationOptions = ({ navigation }) => {

        return {
            headerStyle: {
                backgroundColor: navigation.getParam('headerColor', '#ccc'),
                borderBottomWidth: 0,
                
            },
            headerTintColor: navigation.getParam('tintColor', '#ffffff')

        }
    }



    loadCheckins = () => {

        

        fetch("http://198.211.113.20/checkins?user.username=visaoativa")
        .then ( res => res.json() )
        .then ( res => {
            this.setState({
                data: res || []
            })
        })
    }

    componentDidMount() {
        this.loadCheckins();
    }

  render(){

    const colorBelt = this.state.colorBelt


    if(this.state.loading){
        <View>
             <ActivityIndicator
                size="large"
             >
                 <Text>Aguarde, carregando...</Text>
             </ActivityIndicator>
        </View>
    }
    else{
        
    
    return (

        <SafeAreaView style={styles.contentPage}>
         <View>
            <Text style={styles.headerText}>
                Boa Monstro. Você já frequentou 1.000 aulas de Jiu-Jitsu desde seu início em 12/03/2017.
            </Text>
            <View style={styles.headerCheckin}>
                <View style={styles.countMonth}>
                    <Text>5</Text>
                </View>
                <View style={styles.month}>
                    <Text>Janeiro 2019</Text>
                </View>
            </View>
        <FlatList
        style={styles.flatlist}
            data={this.state.data}
            renderItem={({item}) => (
            
              <View style={styles.gridList}>
                  <View>
                  <Image 
                    style={styles.avatar}
                    source={require('../assets/images/vdl_logo.png')}
                    />
                  </View>
                  <View>
                    <Text style={styles.subtitle}>Check-in: {item.data}</Text>
                    <Text style={styles.subtitle}>Confirmação: {item.data}</Text>
                  </View>
                  <View style={styles.viewStatus}>
                    <Text style={styles.confirmado}>OK</Text>
                  </View>
              </View>
              
                  
              
        )}
        >
        </FlatList>
        </View>
      </SafeAreaView>
    )
    }
  }
}