import React, { Component } from 'react';
import { Text, Image, TextInput, SafeAreaView, TouchableOpacity, View } from 'react-native';

import styles from '../assets/css/Register';

export default class Register extends Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                
                <TextInput 
                    placeholder="E-mail"
                    style={styles.input}
                    placeholderTextColor="#FFF"
                />
                <TouchableOpacity style={styles.botaoLogin}>
                    <Text style={styles.textoLogin}>Recuperar</Text>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
}