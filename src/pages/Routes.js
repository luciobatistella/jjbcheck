import React from 'react';
import api from '../services/api'
import {AsyncStorage} from 'react-native'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';



import Login from './Login'
import Register from './Register'
import ForgotPass from './ForgotPass'
import Dashboard from './Dashboard'
import HistoyCheckin from './HistoyCheckin'
import ModalityDetail from './ModalityDetail'

console.log('teste');
const headerColor = async () => {
    const color = await AsyncStorage.getItem('@CodeApi:headerColor')
    console.log('header:',color);
    color = color.toString()
    return color
}

   

const AppNavigator = createStackNavigator({
    
    
    Login: {
        screen: Login,
        navigationOptions: {
            header: null
          }
    },
    Register: {
        screen: Register,
        navigationOptions: {
            title: 'Criar uma nova conta',
            headerStyle: {
                backgroundColor: '#02AEF1',
              },
              headerTintColor: '#fff',
             
          },
          
    },
    ForgotPass: {
        screen: ForgotPass,
        navigationOptions: {
            title: 'Esqueci a Senha',
            headerStyle: {
                backgroundColor: '#02AEF1',
              },
              headerTintColor: '#fff',
             
          },
    },
    Dashboard: {
        screen: Dashboard,
        navigationOptions: {
            title: 'Dashboard',
            
            headerStyle: {
                backgroundColor: '#02AEF1',
                borderBottomWidth: 0,
    
              },
             
          }
},
    ModalityDetail: {
        screen: ModalityDetail,
        

    },
    HistoyCheckin: {
        screen: HistoyCheckin,
        navigationOptions: {
            title: 'Histórico de Presença',
            headerStyle: {
                backgroundColor: '#0172CC',
              },
               headerTintColor: '#fff',
             
          },
    }
});




export default createAppContainer(AppNavigator);