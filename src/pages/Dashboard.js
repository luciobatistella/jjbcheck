import React, { Component } from 'react';
import { Text, Image, ScrollView, SafeAreaView,AsyncStorage, KeyboardAvoidingView, View, TouchableOpacity } from 'react-native';
import api from '../services/api'

// IMPORT DO CSS
import styles from '../assets/css/Dashboard';
import { log } from 'react-native-reanimated';

export default class Register extends Component {


    constructor(props){
        super(props);
        this.state ={
            data: [],
            isFetch: true,
            meuUsuario: {},
            beltColor:'',
            modalities: [],
            nextDegree:'',
            colorHeader:'',
            graduations:null,
            beltColor:''
        }
    }

   

  
       
        
    

   
    componentDidMount() {
       
        this.getMyuser()

      //  this.loadDashboard();
    } 


    async getMyuser(){
        var beltColor
        const myUser = await AsyncStorage.getItem('@CodeApi:myUser')
        const myToken = await AsyncStorage.getItem('@CodeApi:myToken')
        const usr = JSON.parse(myUser)
   
      
        
        try {
            const usuario = await api.get(`registrations?user.id=${usr.id}`)
            const user = usuario.data[0]
            console.log(user);
            
           await this.setState({modalities: user.modalities})
           await this.setState({nextDegree: user.nextDegree})
           await this.setState({graduations: user.graduations.length})
            console.log(user.graduations.length);
            
            beltColor = user.currentBelt
            this.setState({meuUsuario: user})
       ;
       
       
        try {
            const belt = await api.get(`belts?colorBelt=${beltColor}`)

            this.props.navigation.setParams({ headerColor: belt.data[0].hexColor })
            await AsyncStorage.setItem('@CodeApi:headerColor', belt.data[0].hexColor)
            this.setState({colorBelt: belt.data[0].hexColor})
            this.setState({beltColor: belt.data[0].colorBelt})
        } catch (error) {
            
        
    }
          
          
        } catch (error) {
        }

        
        
    }

async logOut() {
    await AsyncStorage.setItem('@CodeApi:myToken', '')
    await AsyncStorage.setItem('@CodeApi:myUser', '')

    this.props.navigation.navigate('Login')
}

static navigationOptions = ({navigation}) => {
   
    return{
    headerStyle: {
        backgroundColor: navigation.getParam('headerColor', '#2196F3'),
      },
      headerTintColor: '#fff',

}
}

    render() {
        const nextDegree = this.state.nextDegree
        const beltColor = this.state.beltColor
        const modalities = this.state.modalities
        const colorBelt= this.state.colorBelt
        const meuUsuario = this.state.meuUsuario
        const graduations = this.state.graduations
        return (
            
            <SafeAreaView style={styles.container}>
                <View style={styles.containerFull}>
                    {/* <View style={styles.viewUserHeader}>
                        <Image 
                        style={styles.avatar}
                        source={require('../assets/images/avatar.png')}
                        onPress={() => this.logOut()}
                        ></Image>
                        <View>
                            <Text style={styles.welcome}>Boa Noite</Text>
                            <Text style={styles.nameUser}>{meuUsuario.fullName}</Text>
                        </View>
                    </View> 
                    <View style={styles.horizontalRule}/> */}
                    {/* <Text style={styles.Title}>Modalidades</Text> */}
                    
                <ScrollView style={styles.ScrollViewList}>

                {this.state.modalities.map((modality, index) => {
                    var nameModality = modality.nameModality
                   return <View key={index}>
                <TouchableOpacity style={styles.listModality} shadowOpacity={ 0.5 } shadowRadius={ 10 } onPress={() => this.props.navigation.navigate('ModalityDetail',{modality : modality,  beltColor:beltColor, nextDegree:nextDegree, colorBelt:colorBelt, graduations:graduations})}>
                    <Image 
                        style={styles.imageModality}
                        source={require('../assets/images/modality-jiujitsu.png')}
                    >
                    </Image>
                    <View>
                        <Text style={styles.TitleModality}>{nameModality}</Text>
                        <Text style={styles.beltActive}>Faixa {beltColor}</Text>
                        <Text style={styles.nextDegree}>Próximo Grau: {nextDegree} aulas</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.listModality} shadowOpacity={ 0.5 } shadowRadius={ 10 } disabled>
                    <Text style={styles.imageModalityIcon}>🎬</Text>
                    <View>
                        <Text style={styles.TitleModality}>Vídeo Aula</Text>
                        <Text style={styles.beltActive}>Demian Maia</Text>
                        <Text style={styles.nextDegree}>Jiu-Jitsu Brasileiro</Text>
                    </View>
                </TouchableOpacity>
                </View>
                })}
                   
                </ScrollView>
                </View>      
            </SafeAreaView>
        )
    }
}