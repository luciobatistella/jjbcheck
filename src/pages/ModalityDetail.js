import React, { Component } from 'react';
import { Text, Image, ScrollView, AsyncStorage, SafeAreaView, KeyboardAvoidingView, View, TouchableOpacity, Button } from 'react-native';
import ProgressCircle from 'react-native-progress-circle';
import api from '../services/api'


// IMPORT DO CSS
import styles from '../assets/css/ModalityDetail';

export default class Register extends Component {



    constructor(props) {
        super(props);
        this.state = {
            modality: {},
            myUser: {},
            nextDegree: '',
            colorBelt: '',
            beltColor: '',
            need: null,
            percentage: null,
            checkins: null,
            hadGraduated: false,
            lastGraduation: null
        };
    }


    static navigationOptions = ({ navigation }) => {

        return {
            title: navigation.getParam('mobility', 'Modalidade'),
            headerStyle: {
                backgroundColor: navigation.getParam('headerColor', '#000'),
                borderBottomWidth: 0,
                
            },
            headerTintColor: navigation.getParam('tintColor', '#ffffff'),
            
        }
    }


    componentDidMount() {

        this.getParams()
        this.getGraduations()


    };

    async getParams() {

        const beltColor = this.props.navigation.state.params.beltColor
        this.setState({ beltColor: beltColor })

        const colorBelt = this.props.navigation.state.params.colorBelt
        this.setState({ colorBelt: colorBelt })
        this.props.navigation.setParams({ headerColor: this.props.navigation.state.params.colorBelt })


        this.props.navigation.setParams({ tintColor: '#FFF' })

        const graduations = this.props.navigation.state.params.graduations
        this.setState({ graduations: graduations })

        const modality = this.props.navigation.state.params.modality
        this.setState({ modality: modality })
        this.props.navigation.setParams({ mobility: modality.nameModality })

        const nextDegree = this.props.navigation.state.params.nextDegree
        this.setState({ nextDegree: nextDegree })

        const graduation = await api.get(`belts?colorBelt=${beltColor}`)

        const myUser = await AsyncStorage.getItem('@CodeApi:myUser')
        const usr = JSON.parse(myUser)


        try {
            const checkins = await api.get(`checkins?user.id=${usr.id}`)

            this.setState({ checkins: checkins.data.length })
            this.calculateGraduations(graduation.data[0], nextDegree);

        } catch (error) {

        }
    }

    async calculateGraduations(graduation, nextDegree) {
        const checkins = this.state.checkins

        let aulas = graduation.receiveDegree - checkins
        console.log(aulas);

        let percentual
        percentual = (checkins / graduation.receiveDegree) * 100
        this.setState({ percentage: parseFloat(percentual) })
        this.setState({ need: aulas })
        console.log(percentual);

    }

    async getGraduations() {
        const myUser = await AsyncStorage.getItem('@CodeApi:myUser')
        const usr = JSON.parse(myUser)
        const beltColor = this.props.navigation.state.params.beltColor
        try {
            const graduations = await api.get(`graduations?user.id=${usr.id}&belts.colorBelt=${beltColor}`)
            let graduation = graduations.data[0]


            if (!graduation) {
                this.setState({ hadGraduated: false })
            } else {
                console.log(graduation);
                this.setState({ hadGraduated: true })
                if (graduation.firstGraduation && !graduation.secondGraduation && !graduation.thirdGraduation && !graduation.fourthGraduation) {
                
                    let data = this.convertDate(graduation.firstGraduation)
                    this.setState({lastGraduation: data})
                }
                if (graduation.firstGraduation && graduation.secondGraduation && !graduation.thirdGraduation && !graduation.fourthGraduation) {
                    let data = this.convertDate(graduation.secondGraduation)
                    this.setState({lastGraduation: data})
                   
                }
                if (graduation.firstGraduation && graduation.secondGraduation && graduation.thirdGraduation && !graduation.fourthGraduation) {
                    let data = this.convertDate(graduation.thirdGraduation)
                    this.setState({lastGraduation: data})
                }
                if (graduation.firstGraduation && graduation.secondGraduation && graduation.thirdGraduation && graduation.fourthGraduation) {
                    let data = this.convertDate(graduation.fourthGraduation)
                    this.setState({lastGraduation: data})
                }
            }
        } catch (error) {

        }

    }

    async doCheckin() {

        const myUser = await AsyncStorage.getItem('@CodeApi:myUser')
        const usr = JSON.parse(myUser)

      const  dados = {
            user: usr,

        }
       try {
        const checkin = await api.post('checkins', dados)

        console.log(checkin.data);
        
       } catch (error) {
           console.log(error);
           
       }

    ;
        
    }
    async history()  {
        this.props.navigation.navigate('HistoyCheckin')
    }

    convertDate(dateString){
        var p = dateString.split(/\D/g)
        let retorno
        retorno =  [p[2],p[1],p[0] ].join("-").replace(["-"], "/",).replace("-", "/")

        return retorno
        }

    render() {
        const nextDegree = this.state.nextDegree
        const colorBelt = this.state.colorBelt
        const graduations = this.state.graduations
        const beltColor = this.state.beltColor
        const percentage = this.state.percentage
        const need = this.state.need
        const hadGraduated = this.state.hadGraduated
        const lastGraduation = this.state.lastGraduation
        return (
            <View style={[styles.viewArea, { backgroundColor: colorBelt,  }]}>
                <SafeAreaView style={styles.container}>
                <KeyboardAvoidingView style={styles.headerUser}>
                    <View style={styles.viewUserHeader}>
                        <View>
                            <Text style={styles.congratulations}>Parabéns Monstro !!!!</Text>
                            <Text onPress={() => this.history()}>123</Text>
                            {graduations === 1 ? (
                                <Text style={styles.msgInfo}>Você é um Faixa {beltColor} com {graduations} grau.</Text>
                            ) : (
                                    <Text style={styles.msgInfo}>Você é um Faixa {beltColor} com {graduations} graus.</Text>

                                )}

                        </View>
                    </View>
                    <View style={styles.horizontalRule} />
                    <View style={[styles.blueBelt, { backgroundColor: colorBelt }]}>
                        {graduations === 1 ? (
                            <View style={styles.blacktip}>
                                <View style={styles.degree1}>

                                </View>
                            </View>
                        ) : graduations === 2 ? (
                            <View style={styles.blacktip}>
                                <View style={styles.degree1}>
                                    <View style={styles.degree2}>

                                    </View>
                                </View>
                            </View>
                        ) : graduations === 3 ? (
                            <View style={styles.blacktip}>
                                <View style={styles.degree1}>
                                    <View style={styles.degree2}>
                                        <View style={styles.degree3}>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        ) : graduations === 4 ? (
                            <View style={styles.blacktip}>
                                <View style={styles.degree1}>
                                    <View style={styles.degree2}>
                                        <View style={styles.degree3}>
                                            <View style={styles.degree4}></View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        ) : graduations === 0 ? (
                            <View style={styles.blacktip}>
                            </View>
                        ) : (
                                                <View style={styles.blacktip}>
                                                </View>
                                            )}

                    </View>
                    <Text style={styles.congratulations}>Próximo Grau</Text>
                    <Text style={styles.msgInfo}>Monstro, é muito bom ver sua evolução nos treinos, continue assim. Daqui a <Text style={styles.nextDegreeMsg}>{need}</Text> aulas, você vai receber um grau. Oss.</Text>
                    <View style={styles.progressChart}>
                        <ProgressCircle
                            percent={percentage}
                            radius={140}
                            borderWidth={30}
                            color={colorBelt}
                            shadowColor="#666"
                            bgColor="#fff"
                        >
                            <Text style={{ fontSize: 48 }}>{need}</Text>
                        </ProgressCircle>
                    </View>
                    {!hadGraduated ? (
                        <Text style={styles.lastDegree}>você ainda não tem graduacão</Text>
                    ) : (
                            <Text style={styles.lastDegree}>Última Graduação: {lastGraduation}</Text>
                        )}

                    <TouchableOpacity
                    onPress={() => this.doCheckin()}
                        style={[styles.botaoCheckin]}
                    >
                        <Text style={styles.textoCheckin}>Marcar Presença</Text>
                    </TouchableOpacity>

                </KeyboardAvoidingView>
            </SafeAreaView>
            </View>
        )
    }
}
