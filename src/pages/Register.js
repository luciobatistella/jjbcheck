import React, { Component } from 'react';
import { Text, Image, TextInput, KeyboardAvoidingView, TouchableOpacity, View } from 'react-native';
import api from '../services/api'

import styles from '../assets/css/Register';
import { log } from 'react-native-reanimated';

export default class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            email: '',
            password: '',
            birth: '',
            phone: '',

        };
    }

    signUp = async () => {
        const email = this.state.email
        const username = this.state.username
        const phone = this.state.phone
        const password = this.state.password
        const birth = this.state.birth

        var user = {
            username,
            phone,
            email,
            password,
            birth
        }
       
        console.log(user);

        try {
            const response = await api.post('users', user);
            console.log(response);

        } catch (error) {
            console.log(error);

        }
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View style={styles.viewAvatar}>
                    <Image
                        style={styles.avatar}
                        source={require('../assets/images/avatar.png')}
                    />
                </View>
                <TextInput
                    onChangeText={text => this.setState({ username: text })}
                    placeholder="Usuário"
                    placeholderTextColor="#FFF"
                    style={styles.input}
                />
                <TextInput
                    onChangeText={text => this.setState({ email: text })}
                    placeholder="E-mail"
                    keyboardType="email-address"
                    placeholderTextColor="#FFF"
                    style={styles.input}

                />
                <TextInput
                onChangeText={text => this.setState({ password: text })}
                    placeholder="Senha"
                    style={styles.input}
                    secureTextEntry={true}
                    placeholderTextColor="#FFF"
                />
                <TextInput
                    onChangeText={text => this.setState({ phone: text })}
                    placeholder="Celular"
                    style={styles.input}
                    keyboardType={'numeric'}
                    placeholderTextColor="#FFF"
                />
                <TextInput
                    onChangeText={text => this.setState({ birth: text })}
                    placeholder="Data Nascimento"
                    style={styles.input}
                    keyboardType={'numeric'}
                    placeholderTextColor="#FFF"

                />
                <TouchableOpacity
                    onPress={() => this.signUp()}
                    style={styles.botaoLogin}>
                    <Text style={styles.textoLogin}>Cadastrar</Text>
                </TouchableOpacity>
            </KeyboardAvoidingView>
        )
    }
}