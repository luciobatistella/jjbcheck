import React, { Component } from 'react';
import { View, Text, TextInput, Image, TouchableOpacity, KeyboardAvoidingView, AsyncStorage, StatusBar } from 'react-native';
import api from '../services/api'

import styles from '../assets/css/Login';

export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      identifier: '',
      password: '',
    };
  }

  login = async () => {
    const identifier = this.state.identifier
    const password = this.state.password
  
    var user = {
      identifier,
      password,
    }

    console.log(user);
 
    try {

      const response = await api.post('auth/local', user);
      console.log(response.data);
      const token = response.data;

      let myUser = JSON.stringify(token.user) 
       let myToken = token.jwt

     
      

      await AsyncStorage.setItem('@CodeApi:myToken', myToken)
      await AsyncStorage.setItem('@CodeApi:myUser', myUser)

      this.props.navigation.navigate('Dashboard')
    } catch (error) {
      alert('Credenciais inválidas', 'Usuário não encontrado')
    }
  }

  async componentDidMount() {
    const token = await AsyncStorage.getItem('@CodeApi:myToken')
    if(token){
       
        
        this.props.navigation.navigate('Dashboard')
    }
}
  render() {
    return (
      
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <StatusBar barStyle="light-content" />
        <Image
          style={styles.avatar}
          source={require('../assets/images/vdl_logo.png')}
        />
        <TextInput
          onChangeText={text => this.setState({ identifier: text })}
          autoCapitalize={'none'}
          placeholder="Digite seu email"
          style={styles.input}
        />
        <TextInput
          onChangeText={text => this.setState({ password: text })}
          placeholder="Digite seu senha"
          style={styles.input}
          secureTextEntry={true}
        />
        <View style={styles.viewRegisterForgot}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
            <Text style={styles.register}>Cadastre-se</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotPass')}>
            <Text style={styles.forgotpass}>Esqueci a senha</Text>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          onPress={() => this.login()}
          style={styles.botaoLogin}
        >
          <Text style={styles.textoLogin}>Entrar</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
      
    )
  }
}