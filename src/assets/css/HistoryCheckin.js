import {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({

    contentPage: {
        backgroundColor: "#0172CC",
    },
    
    flatlist: {
        height: 1000,
        // backgroundColor: '#FFF'
        
    },
    headerCheckin: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: "#EBEBEB",
        height: 40,
        padding: 10,
    },
    month:{
        marginLeft: "auto",
        marginRight: 10,
    },
    countMonth: {
        marginLeft: 10,
    },

    headerText: {
        padding: 20,
        color: '#505050',
    },

    gridList: {
        height: 70,
        borderBottomColor: "#DAD9DA",
        borderBottomWidth: 1,
        
        alignItems: 'center',
        display: "flex",
        flexDirection: 'row',
        alignItems: 'center',
    },
    avatar: {
        width: 50,
        height: 50,
        marginRight: 10,
        marginLeft: 10,
    },
    subtitle: {
        color: "#A29F9F",
        fontSize: 12,

    },
    viewStatus: {
        marginLeft: "auto",
        marginRight: 20,
    },

    confirmado: {
        backgroundColor: "#078911",
        borderRadius: 50,
        fontSize: 12,
        color: "#FFF",
        padding: 5,
    }



});

export default styles;