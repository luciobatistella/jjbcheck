import {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({

    container: {
        flex:1,
        backgroundColor: "#02AEF1",
        
    },

    containerFull: {
        marginLeft: 20,
        marginRight: 20,
    },

    viewUserHeader: {
        flexDirection: 'row',
        alignItems: 'stretch',
        marginBottom: 5
    },

    viewAvatar: {
        borderWidth:1,
       borderColor:'rgba(0,0,0,0.2)',
       alignItems:'center',
       justifyContent:'center',
       width:50,
       height:50,
       backgroundColor:'#fff',
       borderRadius:50,
    },

    avatar: {
        width: 35,
        height: 35,
        marginRight: 10
    },

    welcome: {
        fontSize: 10,
        marginBottom: 2,
        color: '#505050',
    },

    nameUser: {
        fontSize: 16,
        color: '#000000',
    }, 
    horizontalRule:{
        borderBottomColor: '#DEDEDE',
        borderBottomWidth: 1,
        marginBottom: 20   
    },
    Title: {
        fontSize: 16,
        color: '#505050',
    },
    TitleModality: {
        fontSize: 20,
        color: '#505050',
        textAlign: 'center',
    },
    beltActive: {
        fontSize: 12,
        marginBottom: 2,
        color: '#505050',
        textAlign: 'center',   
        marginTop: 5, 
    },
    nextDegree: {
        fontSize: 12,
        marginBottom: 2,
        color: '#969696',
        textAlign: 'center', 
        marginTop: 10,   
    },

    listModality: {
        elevation: 4,
        backgroundColor: '#F2F2F2',
        height: 120,
        borderRadius: 5,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        borderStyle: 'solid', 
        borderWidth: 1,
        borderColor: '#efefef',
        
        
    },

    ScrollViewList: {
        marginBottom: 40,
    },

    imageModality: {
        width: 85,
        height: 85,
        marginLeft: 30,
        marginRight: 30,
        alignItems:'center',
        justifyContent:'center',
    },
    imageModalityIcon: {
        marginLeft: 40,
        marginRight: 40,
        fontSize: 60,
        alignItems:'center',
        justifyContent:'center',
    }




});

export default styles;