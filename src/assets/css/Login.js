import {
  StyleSheet
} from 'react-native';

const styles = StyleSheet.create({

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#059EE0"
  },
  avatar: {
    width: 150,
    height: 150,
    // marginTop: 80,
    marginBottom: 30
  },
  input: {
    marginTop: 10,
    backgroundColor: '#FFFFFF',
    fontSize: 16,
    padding: 10,
    width: 300,
    height: 50,
    borderRadius: 3,
    color: '#000',
  },
  botaoLogin: {
    width: 300,
    height: 50,
    borderRadius: 3,
    backgroundColor: '#000',
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textoLogin: {
    color: '#FFF',
    fontSize: 16,
    fontWeight: 'bold',
    textTransform: 'uppercase'
  },
  forgotpass: {
    textAlign: 'right',
    width: 150,
    color: '#FFF',
    marginTop: 20,
    fontWeight: 'bold',
    textTransform: "uppercase"
  },
  register: {
    textAlign: 'center',
    width: 300,
    color: '#FFF',
    marginTop: 20,
    fontWeight: 'bold',
  }, 
  viewRegisterForgot: {
    width: 300,
    flexDirection: 'row',
  },
  register: {
    textAlign: 'left',
    width: 150,
    color: '#FFF',
    marginTop: 20,
    fontWeight: 'bold',
    textTransform: "uppercase"
  },
  

});

export default styles;