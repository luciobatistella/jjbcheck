import {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({

    viewArea: {
        height: 1000,
        flex: 1,
    },

    container: {
        backgroundColor: "#FFF",
        alignSelf: 'stretch',
        textAlign: 'center',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 50,
        borderRadius: 5,
        flex: 1,
    },

    viewUserHeader: {
        flexDirection: 'row',
        alignItems: 'stretch',
        marginBottom: 5,
        marginTop: 10,
    },

    viewAvatar: {
        borderWidth:1,
       borderColor:'rgba(0,0,0,0.2)',
       alignItems:'center',
       justifyContent:'center',
       width:50,
       height:50,
       backgroundColor:'#fff',
       borderRadius:50,
    },

    avatar: {
        width: 35,
        height: 35,
        marginRight: 10
    },

    congratulations: {
        fontSize: 10,
        marginBottom: 2,
        color: '#505050',
        marginLeft: 20,
        marginRight: 20,
    },

    msgInfo: {
        fontSize: 16,
        color: '#505050',
        marginLeft: 20,
        marginRight: 20,
    }, 
    horizontalRule:{
        borderBottomColor: '#DEDEDE',
        borderBottomWidth: 1,
        marginLeft: 20,
        marginRight: 20,

    },
    Title: {
        fontSize: 16,
        color: '#505050',
    },
    TitleModality: {
        fontSize: 20,
        color: '#505050',
        textAlign: 'center',
    },
    beltActive: {
        fontSize: 12,
        marginBottom: 2,
        color: '#505050',
        textAlign: 'center',   
        marginTop: 5, 
        
    },
    nextDegree: {
        fontSize: 12,
        marginBottom: 2,
        color: '#969696',
        textAlign: 'center', 
        marginTop: 10,   
    },
    nextDegreeMsg: {
        fontWeight: 'bold',
        color: '#990000',
        fontSize: 14
    },

    listModality: {
        backgroundColor: '#F2F2F2',
        height: 120,
        borderRadius: 5,
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        borderStyle: 'solid', 
        borderWidth: 1,
        borderColor: '#efefef',
        
    },

    ScrollViewList: {
        marginBottom: 40,
    },

    imageModality: {
        width: 85,
        height: 85,
        marginLeft: 30,
        marginRight: 30,
        alignItems:'center',
        justifyContent:'center',
    },
    progressChart: {
        alignItems:'center',
       justifyContent:'center',
       marginTop: 50,
       marginBottom: 50,
    },
    blueBelt: {
        height: 25,
        borderRadius: 5,
        marginBottom: 10,
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
    },
    blacktip: {
        backgroundColor: '#000',
        height: 25,
        width: 60,
        
        alignSelf: 'flex-end',
    },
    degree1: {
        backgroundColor: '#FFF',
        height: 25,
        width: 5,
        alignSelf: 'flex-end',
        marginRight: 10,
    },
    degree2: {
        backgroundColor: '#FFF',
        height: 25,
        width: 5,
        alignSelf: 'flex-end',
        marginRight: 10,
    },
    degree3: {
        backgroundColor: '#FFF',
        height: 25,
        width: 5,
        alignSelf: 'flex-end',
        marginRight: 10,
    },
    degree4: {
        backgroundColor: '#FFF',
        height: 25,
        width: 5,
        alignSelf: 'flex-end',
        marginRight: 10,
    },
    botaoCheckin: {
        height: 54,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#3E3E3E',
        marginLeft: 20,
        marginRight: 20,
        top: 50,
    },
    textoCheckin: {
        color: '#FFF',
        textAlign: 'center',
        textTransform: "uppercase",
        fontSize: 16,
        fontWeight: "bold",
    },
    lastDegree: {
        fontSize: 12,
        marginBottom: 2,
        color: '#505050',
        textAlign: 'center',   
        marginBottom: 10, 
    }




});

export default styles;