import {
    StyleSheet
} from 'react-native';

const styles = StyleSheet.create({

    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: "#02AEF1"
    },
    input: {
        marginTop: 10,
        backgroundColor: 'transparent',
        borderBottomColor: '#DEDEDE',
        borderBottomWidth: 1,
        color: '#FFF',
        fontSize: 14,
        padding: 10,
        width: 300,
        height: 40,
        borderRadius: 3
    },
    botaoLogin: {
        width: 300,
        height: 50,
        borderRadius: 3,
        backgroundColor: '#000',
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textoLogin: {
        color: '#FFF',
        fontSize: 16,
        fontWeight: 'bold',
        textTransform: 'uppercase'
    },
    avatar: {
        width: 50,
        height: 50,

    },
    viewAvatar: {
        marginBottom: 10,
        marginTop: 100,
        backgroundColor: '#C1C1C1',
        borderRadius: 120,
        width: 120,
        height: 120,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

export default styles;